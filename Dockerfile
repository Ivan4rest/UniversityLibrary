FROM openjdk:11
EXPOSE 8091
ADD target/UniversityLibrary-1.0-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]