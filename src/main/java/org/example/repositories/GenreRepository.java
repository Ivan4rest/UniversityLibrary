package org.example.repositories;

import org.example.entities.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {

    @Query("select g.id from Genre g where g.name = ?1")
    List<Long> getGenresIdByName(String genreName);
}
