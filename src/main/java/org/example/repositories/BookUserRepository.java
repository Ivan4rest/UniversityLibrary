package org.example.repositories;

import org.example.entities.BookUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BookUserRepository extends JpaRepository<BookUser, Long> {

    @Query("select bu.book.id from BookUser bu where bu.userUUID = ?1")
    List<Long> getBooksIdByUserId(String userUUID);

    @Query("select bu.userUUID from BookUser bu where bu.book.id = ?1")
    List<String> getUsersUUIDByBookId(Long bookId);

    @Query("select bu.id from BookUser bu where bu.book.id = ?1 and bu.userUUID = ?2")
    Long getByBookIdAndUserUUID(Long bookId, String userUUID);

    @Transactional
    @Modifying
    @Query("delete from BookUser bu where bu.userUUID = ?1")
    void deleteByUserId(String userUUID);

    @Transactional
    @Modifying
    @Query("delete from BookUser bu where bu.book.id = ?1")
    void deleteByBookId(Long bookId);

    @Transactional
    @Modifying
    @Query("delete from BookUser bu where bu.userUUID = ?1 and bu.book.id = ?2")
    void deleteByUserIdAndBookId(String userUUID, Long bookId);
}
