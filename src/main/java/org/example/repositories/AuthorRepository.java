package org.example.repositories;

import org.example.entities.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

    @Query("select a.id from Author a where a.firstName = ?1")
    List<Long> getAuthorsIdByFirstName(String firstName);

    @Query("select a.id from Author a where a.lastName = ?1")
    List<Long> getAuthorsIdByLastName(String lastName);
}
