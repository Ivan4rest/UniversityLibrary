package org.example.repositories;

import org.example.entities.BookGenre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface BookGenreRepository extends JpaRepository<BookGenre, Long> {

    @Query("select bg.genre.id from BookGenre bg where bg.book.id = ?1")
    List<Long> getGenresIdByBookId(Long bookId);

    @Query("select bg.book.id from BookGenre bg where bg.genre.id = ?1")
    List<Long> getBooksIdByGenreId(Long genreId);

    @Transactional
    @Modifying
    @Query("delete from BookGenre bg where bg.book.id = ?1")
    void deleteByBookId(Long id);

    @Transactional
    @Modifying
    @Query("delete from BookGenre bg where bg.genre.id = ?1")
    void deleteByGenreId(Long id);
}
