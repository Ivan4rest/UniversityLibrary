package org.example.repositories;

import org.example.entities.BookAuthor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface BookAuthorRepository extends JpaRepository<BookAuthor, Long> {

    @Query("select ba.author.id from BookAuthor ba where ba.book.id = ?1")
    List<Long> getAuthorsIdByBookId(Long bookId);

    @Query("select ba.book.id from BookAuthor ba where ba.author.id = ?1")
    List<Long> getBooksIdByAuthorId(Long authorId);

    @Transactional
    @Modifying
    @Query("delete from BookAuthor ba where ba.book.id = ?1")
    void deleteByBookId(Long bookId);

    @Transactional
    @Modifying
    @Query("delete from BookAuthor ba where ba.author.id = ?1")
    void deleteByAuthorId(Long authorId);
}
