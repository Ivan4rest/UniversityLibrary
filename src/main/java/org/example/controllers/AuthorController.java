package org.example.controllers;

import org.example.dto.AuthorDTO;
import org.example.services.AuthorService;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class AuthorController {
    private AuthorService authorService;

    public AuthorService getAuthorService() {
        return authorService;
    }

    @Autowired
    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }

    private String getToken(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getTokenString();
    }

    private Set<String> getRoles(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getToken().getRealmAccess().getRoles();
    }

    @GetMapping("/v0/authors")
    public ResponseEntity<List<AuthorDTO>> getAll(HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<AuthorDTO> authorDTOS = getAuthorService().getAll();

            return authorDTOS != null && !authorDTOS.isEmpty()
                    ? new ResponseEntity<>(authorDTOS, headers, HttpStatus.OK)
                    : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v0/authors/{id}")
    public ResponseEntity<AuthorDTO> getById(@PathVariable Long id,
                                             HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            AuthorDTO authorDTO = getAuthorService().getById(id);

            return authorDTO != null
                    ? new ResponseEntity<>(authorDTO, headers, HttpStatus.OK)
                    : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v0/authors/firstnames/{firstname}")
    public ResponseEntity<List<AuthorDTO>> getAuthorsByFirstname(@PathVariable String firstname,
                                                                 HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<AuthorDTO> authorDTOS = getAuthorService().getAuthorsByFirstname(firstname);

            return authorDTOS != null && !authorDTOS.isEmpty()
                    ? new ResponseEntity<>(authorDTOS, headers, HttpStatus.OK)
                    : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v0/authors/lastnames/{lastname}")
    public ResponseEntity<List<AuthorDTO>> getAuthorsByLastname(@PathVariable String lastname,
                                                                 HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<AuthorDTO> authorDTOS = getAuthorService().getAuthorsByLastname(lastname);

            return authorDTOS != null && !authorDTOS.isEmpty()
                    ? new ResponseEntity<>(authorDTOS, headers, HttpStatus.OK)
                    : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/v0/authors")
    public ResponseEntity<?> add(@RequestBody AuthorDTO authorDTO,
                                 HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin")){
                getAuthorService().add(authorDTO);

                return new ResponseEntity<>(headers, HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }

        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/v0/authors/{id}")
    public ResponseEntity<?> update(@PathVariable Long id,
                                    @RequestBody AuthorDTO authorDTO,
                                    HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin")){
                getAuthorService().update(id, authorDTO);

                return new ResponseEntity<>(headers, HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/v0/authors/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id,
                                    HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin")){
                getAuthorService().deleteById(id);

                return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

}
