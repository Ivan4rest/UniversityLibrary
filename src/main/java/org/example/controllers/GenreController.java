package org.example.controllers;

import org.example.dto.BookDTO;
import org.example.dto.GenreDTO;
import org.example.services.GenreService;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class GenreController {
    private GenreService genreService;

    public GenreService getGenreService() {
        return genreService;
    }

    @Autowired
    public void setGenreService(GenreService genreService) {
        this.genreService = genreService;
    }

    private String getToken(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getTokenString();
    }

    private Set<String> getRoles(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getToken().getRealmAccess().getRoles();
    }

    @GetMapping("/v0/genres")
    public ResponseEntity<List<GenreDTO>> getAll(HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<GenreDTO> genreDTOS = getGenreService().getAll();

            return genreDTOS != null && !genreDTOS.isEmpty()
                    ? new ResponseEntity<>(genreDTOS, headers, HttpStatus.OK)
                    : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v0/genres/{id}")
    public ResponseEntity<GenreDTO> getById(@PathVariable Long id,
                                            HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            GenreDTO genreDTO = getGenreService().getById(id);

            return genreDTO != null
                    ? new ResponseEntity<>(genreDTO, headers, HttpStatus.OK)
                    : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v0/genres/names/{genreName}")
    public ResponseEntity<List<GenreDTO>> getGenresByName(@PathVariable String genreName,
                                                          HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<GenreDTO> genreDTOS = getGenreService().getGenresByName(genreName);

            return genreDTOS != null && !genreDTOS.isEmpty()
                    ? new ResponseEntity<>(genreDTOS, headers, HttpStatus.OK)
                    : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/v0/genres")
    public ResponseEntity<?> add(@RequestBody GenreDTO genreDTO,
                                 HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin")){
                getGenreService().add(genreDTO);

                return new ResponseEntity<>(genreDTO, headers, HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/v0/genres/{id}")
    public ResponseEntity<?> update(@PathVariable Long id,
                                    @RequestBody GenreDTO genreDTO,
                                    HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin")){
                getGenreService().update(id, genreDTO);

                return new ResponseEntity<>(headers, HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/v0/genres/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id,
                                    HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin")){
                getGenreService().deleteById(id);

                return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }
}