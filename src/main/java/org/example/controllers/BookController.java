package org.example.controllers;

import org.example.dto.BookDTO;
import org.example.services.BookService;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class BookController {
    private BookService bookService;

    public BookService getBookService() {
        return bookService;
    }

    @Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    private String getToken(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getTokenString();
    }

    private Set<String> getRoles(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getToken().getRealmAccess().getRoles();
    }

    @GetMapping("/v0/books")
    public ResponseEntity<List<BookDTO>> getAll(HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<BookDTO> bookDTOS = getBookService().getAll();

            return bookDTOS != null && !bookDTOS.isEmpty()
                    ? new ResponseEntity<>(bookDTOS, headers, HttpStatus.OK)
                    : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/v0/books/{id}")
    public ResponseEntity<BookDTO> getById(@PathVariable Long id,
                                           HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            BookDTO bookDTO = getBookService().getById(id);

            return bookDTO != null
                    ? new ResponseEntity<>(bookDTO, headers, HttpStatus.OK)
                    : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v0/books/authors/{authorId}")
    public ResponseEntity<List<BookDTO>> getBooksByAuthorId(@PathVariable Long authorId,
                                                           HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<BookDTO> bookDTOS = getBookService().getBooksByAuthorId(authorId);

            return bookDTOS != null && !bookDTOS.isEmpty()
                    ? new ResponseEntity<>(bookDTOS, headers, HttpStatus.OK)
                    : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v0/books/genres/{genreId}")
    public ResponseEntity<List<BookDTO>> getBooksByGenreId(@PathVariable Long genreId,
                                                           HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<BookDTO> bookDTOS = getBookService().getBooksByGenreId(genreId);

            return bookDTOS != null && !bookDTOS.isEmpty()
                    ? new ResponseEntity<>(bookDTOS, headers, HttpStatus.OK)
                    : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v0/books/titles/{title}")
    public ResponseEntity<List<BookDTO>> getBooksByTitle(@PathVariable String title,
                                                           HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<BookDTO> bookDTOS = getBookService().getBooksByTitle(title);

            return bookDTOS != null && !bookDTOS.isEmpty()
                    ? new ResponseEntity<>(bookDTOS, headers, HttpStatus.OK)
                    : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/v0/books")
    public ResponseEntity<?> add(@RequestBody BookDTO bookDTO,
                                 HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin")){
                getBookService().add(bookDTO);

                return new ResponseEntity<>(headers, HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/v0/books/{id}")
    public ResponseEntity<?> update(@PathVariable Long id,
                                    @RequestBody BookDTO bookDTO,
                                    HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin")){
                getBookService().update(id, bookDTO);

                return new ResponseEntity<>(headers, HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/v0/books/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id,
                                    HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin")){
                getBookService().deleteById(id);

                return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }
}