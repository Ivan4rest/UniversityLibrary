package org.example.controllers;

import org.example.dto.BookDTO;
import org.example.services.BookUserService;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class UserController {
    private BookUserService bookUserService;

    public BookUserService getBookUserService() {
        return bookUserService;
    }

    @Autowired
    public void setBookUserService(BookUserService bookUserService) {
        this.bookUserService = bookUserService;
    }

    private String getToken(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getTokenString();
    }

    private Set<String> getRoles(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getToken().getRealmAccess().getRoles();
    }

    private String getUserUUID(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getToken().getSubject();
    }

    @PostMapping("/v0/users/{userUUID}/books/{bookId}/pages/{numberOfPage}")
    public ResponseEntity<?> addUserBook(@PathVariable String userUUID,
                                         @PathVariable Long bookId,
                                         @PathVariable Integer numberOfPage,
                                         HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(userUUID)){
                getBookUserService().add(userUUID, bookId, numberOfPage);
                return new ResponseEntity<>(headers, HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/v0/users/{userUUID}/books/{bookId}/pages/{numberOfPage}")
    public ResponseEntity<?> updateUserBook(@PathVariable String userUUID,
                                            @PathVariable Long bookId,
                                            @PathVariable Integer numberOfPage,
                                            HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(userUUID)){
                getBookUserService().update(userUUID, bookId, numberOfPage);
                return new ResponseEntity<>(headers, HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/v0/users/{userUUID}/books")
    public ResponseEntity<List<BookDTO>> getBooksByUserId(@PathVariable String userUUID,
                                                          HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(userUUID)){
                List<BookDTO> bookDTOS = getBookUserService().getBooksByUserId(userUUID);

                return bookDTOS != null && !bookDTOS.isEmpty()
                        ? new ResponseEntity<>(bookDTOS, headers, HttpStatus.OK)
                        : new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/v0/users/{userUUID}/books/{bookId}")
    public ResponseEntity<?> deleteUserBookByUserIdAndBookId(@PathVariable String userUUID,
                                                             @PathVariable Long bookId,
                                                             HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(userUUID)){
                getBookUserService().deleteByUserIdAndBookId(userUUID, bookId);

                return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/v0/users/{userUUID}/books")
    public ResponseEntity<?> deleteUserBooksByUserId(@PathVariable String userUUID,
                                                     HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(userUUID)){
                getBookUserService().deleteByUserId(userUUID);

                return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }
}
