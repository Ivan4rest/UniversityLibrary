package org.example.services;

import org.example.dto.BookDTO;
import org.example.entities.Book;
import org.example.entities.BookAuthor;
import org.example.entities.BookGenre;
import org.example.repositories.AuthorRepository;
import org.example.repositories.BookRepository;
import org.example.repositories.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookService {

    private BookRepository bookRepository;
    private AuthorRepository authorRepository;
    private GenreRepository genreRepository;
    private BookAuthorService bookAuthorService;
    private BookGenreService bookGenreService;
    private BookUserService bookUserService;

    public BookRepository getBookRepository() {
        return bookRepository;
    }

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public AuthorRepository getAuthorRepository() {
        return authorRepository;
    }

    @Autowired
    public void setAuthorRepository(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public GenreRepository getGenreRepository() {
        return genreRepository;
    }

    @Autowired
    public void setGenreRepository(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    public BookAuthorService getBookAuthorService() {
        return bookAuthorService;
    }

    @Autowired
    public void setBookAuthorService(BookAuthorService bookAuthorService) {
        this.bookAuthorService = bookAuthorService;
    }

    public BookGenreService getBookGenreService() {
        return bookGenreService;
    }

    @Autowired
    public void setBookGenreService(BookGenreService bookGenreService) {
        this.bookGenreService = bookGenreService;
    }

    public BookUserService getBookUserService() {
        return bookUserService;
    }

    @Autowired
    public void setBookUserService(BookUserService bookUserService) {
        this.bookUserService = bookUserService;
    }

    public static Book getBookFromBookDTO(BookDTO bookDTO){
        return new Book(bookDTO.getId(), bookDTO.getTitle(), bookDTO.getPublicationYear(), bookDTO.getLanguage(), bookDTO.getFileId(), bookDTO.getDirectoryId(), bookDTO.getNumberOfPages());
    }


    public List<BookDTO> getAll(){
        List<BookDTO> bookDTOS = new ArrayList<>();
        List<Book> books = getBookRepository().findAll();
        for (Book book: books) {
            List<Long> authorsId = bookAuthorService.getAuthorsIdByBookId(book.getId());
            List<Long> genresId = bookGenreService.getGenresIdByBookId(book.getId());
            bookDTOS.add(new BookDTO(book, authorsId, genresId));
        }
        return bookDTOS;
    }

    public BookDTO getById(Long id){
        Book book = getBookRepository().findById(id).get();
        List<Long> authorsId = bookAuthorService.getAuthorsIdByBookId(book.getId());
        List<Long> genresId = bookGenreService.getGenresIdByBookId(book.getId());
        return new BookDTO(book, authorsId, genresId);
    }

    public List<BookDTO> getBooksByAuthorId(Long authorId){
        List<Long> booksId = getBookAuthorService().getBooksIdByAuthorId(authorId);
        List<BookDTO> bookDTOS = new ArrayList<>();
        for (Long bookId:booksId) {
            bookDTOS.add(getById(bookId));
        }
        return bookDTOS;
    }

    public List<BookDTO> getBooksByGenreId(Long genreId){
        List<Long> booksId = getBookGenreService().getBooksIdByGenreId(genreId);
        List<BookDTO> bookDTOS = new ArrayList<>();
        for (Long bookId:booksId) {
            bookDTOS.add(getById(bookId));
        }
        return bookDTOS;
    }

    public List<BookDTO> getBooksByTitle(String title){
        List<Long> booksId = getBookRepository().getBooksIdByTitle(title);
        List<BookDTO> bookDTOS = new ArrayList<>();
        for (Long bookId:booksId) {
            bookDTOS.add(getById(bookId));
        }
        return bookDTOS;
    }

    @Transactional
    public void add(BookDTO bookDTO){
        Book book = getBookRepository().save(getBookFromBookDTO(bookDTO));
        for(Long authorId: bookDTO.getAuthorsId()) {
            BookAuthor bookAuthor = new BookAuthor(book, getAuthorRepository().findById(authorId).get());
            getBookAuthorService().add(bookAuthor);
        }
        for(Long genreId: bookDTO.getGenresId()) {
            BookGenre bookGenre = new BookGenre(book, getGenreRepository().findById(genreId).get());
            getBookGenreService().add(bookGenre);
        }
    }

    @Transactional
    public void update(Long id, BookDTO bookDTO){
        Book book = getBookFromBookDTO(bookDTO);
        Book bookUpdate = getBookRepository().findById(id).get();
        bookUpdate.setTitle(book.getTitle());
        bookUpdate.setPublicationYear(book.getPublicationYear());
        bookUpdate.setLanguage(book.getLanguage());
        bookUpdate.setFileId(book.getFileId());
        getBookAuthorService().deleteByBookId(id);
        getBookGenreService().deleteByBookId(id);
        getBookRepository().save(bookUpdate);
        for(Long authorId: bookDTO.getAuthorsId()) {
            BookAuthor bookAuthor = new BookAuthor(getBookFromBookDTO(bookDTO), getAuthorRepository().findById(authorId).get());
            getBookAuthorService().add(bookAuthor);
        }
        for(Long genreId: bookDTO.getGenresId()) {
            BookGenre bookGenre = new BookGenre(getBookFromBookDTO(bookDTO), getGenreRepository().findById(genreId).get());
            getBookGenreService().add(bookGenre);
        }
    }

    @Transactional
    public void deleteById(Long id){
        getBookAuthorService().deleteByBookId(id);
        getBookGenreService().deleteByBookId(id);
        getBookUserService().deleteByBookId(id);
        getBookRepository().deleteById(id);
    }
}
