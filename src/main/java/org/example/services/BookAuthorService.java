package org.example.services;

import org.example.entities.BookAuthor;
import org.example.repositories.BookAuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookAuthorService {
    private BookAuthorRepository bookAuthorRepository;

    public BookAuthorRepository getBookAuthorRepository() {
        return bookAuthorRepository;
    }

    @Autowired
    public void setBookAuthorRepository(BookAuthorRepository bookAuthorRepository) {
        this.bookAuthorRepository = bookAuthorRepository;
    }

    public List<BookAuthor> getAll(){
        return getBookAuthorRepository().findAll();
    }

    public BookAuthor getById(Long id){
        return getBookAuthorRepository().findById(id).get();
    }

    public List<Long> getAuthorsIdByBookId(Long bookId){
        return bookAuthorRepository.getAuthorsIdByBookId(bookId);
    }

    public List<Long> getBooksIdByAuthorId(Long authorId){
        return bookAuthorRepository.getBooksIdByAuthorId(authorId);
    }

    @Transactional
    public void add(BookAuthor bookAuthor){
        getBookAuthorRepository().save(bookAuthor);
    }

    @Transactional
    public void deleteById(Long id){
        getBookAuthorRepository().deleteById(id);
    }

    @Transactional
    public void deleteByBookId(Long id){
        getBookAuthorRepository().deleteByBookId(id);
    }

    @Transactional
    public void deleteByAuthorId(Long id){
        getBookAuthorRepository().deleteByAuthorId(id);
    }
}
