package org.example.services;

import org.example.dto.GenreDTO;
import org.example.entities.Genre;
import org.example.repositories.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class GenreService {

    private GenreRepository genreRepository;
    private BookGenreService bookGenreService;

    public GenreRepository getGenreRepository() {
        return genreRepository;
    }

    @Autowired
    public void setGenreRepository(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    public BookGenreService getBookGenreService() {
        return bookGenreService;
    }

    @Autowired
    public void setBookGenreService(BookGenreService bookGenreService) {
        this.bookGenreService = bookGenreService;
    }

    public static Genre getGenreFromGenreDto(GenreDTO genreDTO){
        return new Genre(genreDTO.getId(), genreDTO.getName());
    }

    public List<GenreDTO> getAll(){
        List<GenreDTO> genreDTOS = new ArrayList<>();
        List<Genre> genres = getGenreRepository().findAll();
        for (Genre genre: genres) {
            List<Long> genresId = bookGenreService.getBooksIdByGenreId(genre.getId());
            genreDTOS.add(new GenreDTO(genre.getId(), genre.getName(), genresId));
        }
        return genreDTOS;
    }

    public GenreDTO getById(Long id){
        Genre genre = getGenreRepository().findById(id).get();
        List<Long> genresId = bookGenreService.getBooksIdByGenreId(genre.getId());
        return new GenreDTO(genre, genresId);
    }

    public List<GenreDTO> getGenresByName(String genreName){
        List<GenreDTO> genreDTOS = new ArrayList<>();
        List<Long> genresId = getGenreRepository().getGenresIdByName(genreName);
        for (Long genreId: genresId) {
            genreDTOS.add(getById(genreId));
        }
        return genreDTOS;
    }

    @Transactional
    public void add(GenreDTO genreDTO){
        getGenreRepository().save(getGenreFromGenreDto(genreDTO));
    }

    @Transactional
    public void update(Long id, GenreDTO genreDTO){
        Genre genre = getGenreFromGenreDto(genreDTO);
        Genre genreUpdate = getGenreRepository().findById(id).get();
        genreUpdate.setName(genre.getName());
        getGenreRepository().save(genreUpdate);
    }

    @Transactional
    public void deleteById(Long id){
        getBookGenreService().deleteByGenreId(id);
        getGenreRepository().deleteById(id);
    }
}
