package org.example.services;

import org.example.entities.BookGenre;
import org.example.repositories.BookGenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookGenreService {
    private BookGenreRepository bookGenreRepository;

    public BookGenreRepository getBookGenreRepository() {
        return bookGenreRepository;
    }

    @Autowired
    public void setBookGenreRepository(BookGenreRepository bookGenreRepository) {
        this.bookGenreRepository = bookGenreRepository;
    }

    public List<BookGenre> getAll(){
        return getBookGenreRepository().findAll();
    }

    public BookGenre getById(Long id){
        return getBookGenreRepository().findById(id).get();
    }

    public List<Long> getGenresIdByBookId(Long bookId){
        return bookGenreRepository.getGenresIdByBookId(bookId);
    }

    public List<Long> getBooksIdByGenreId(Long genreId){
        return bookGenreRepository.getBooksIdByGenreId(genreId);
    }

    @Transactional
    public void add(BookGenre bookGenre){
        getBookGenreRepository().save(bookGenre);
    }

    @Transactional
    public void deleteById(Long id){
        getBookGenreRepository().deleteById(id);
    }

    @Transactional
    public void deleteByBookId(Long id){
        getBookGenreRepository().deleteByBookId(id);
    }

    @Transactional
    public void deleteByGenreId(Long id){
        getBookGenreRepository().deleteByGenreId(id);
    }
}
