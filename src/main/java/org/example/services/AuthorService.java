package org.example.services;

import org.example.dto.AuthorDTO;
import org.example.entities.Author;
import org.example.repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthorService {

    private AuthorRepository authorRepository;
    private BookAuthorService bookAuthorService;

    public AuthorRepository getAuthorRepository() {
        return authorRepository;
    }

    @Autowired
    public void setAuthorRepository(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public BookAuthorService getBookAuthorService() {
        return bookAuthorService;
    }

    @Autowired
    public void setBookAuthorService(BookAuthorService bookAuthorService) {
        this.bookAuthorService = bookAuthorService;
    }

    public static Author getAuthorFromAuthorDTO(AuthorDTO authorDTO){
        return new Author(authorDTO.getFirstName(), authorDTO.getLastName(), authorDTO.getPatronymic(), authorDTO.getBirthDate());
    }


    public List<AuthorDTO> getAll(){
        List<AuthorDTO> authorDTOS = new ArrayList<>();
        List<Author> authors = getAuthorRepository().findAll();
        for(Author author: authors){
            List<Long> booksId = bookAuthorService.getBooksIdByAuthorId(author.getId());
            authorDTOS.add(new AuthorDTO(author, booksId));
        }
        return authorDTOS;
    }

    public AuthorDTO getById(Long id){
        Author author = getAuthorRepository().findById(id).get();
        List<Long> booksId = bookAuthorService.getBooksIdByAuthorId(author.getId());
        return new AuthorDTO(author, booksId);
    }

    public List<AuthorDTO> getAuthorsByFirstname(String firstname){
        List<AuthorDTO> authorDTOS = new ArrayList<>();
        List<Long> authorsId = getAuthorRepository().getAuthorsIdByFirstName(firstname);
        for(Long authorId: authorsId){
            authorDTOS.add(getById(authorId));
        }
        return authorDTOS;
    }

    public List<AuthorDTO> getAuthorsByLastname(String lastname){
        List<AuthorDTO> authorDTOS = new ArrayList<>();
        List<Long> authorsId = getAuthorRepository().getAuthorsIdByLastName(lastname);
        for(Long authorId: authorsId){
            authorDTOS.add(getById(authorId));
        }
        return authorDTOS;
    }

    @Transactional
    public void add(AuthorDTO authorDTO){
        getAuthorRepository().save(getAuthorFromAuthorDTO(authorDTO));
    }

    @Transactional
    public void update(Long id, AuthorDTO authorDTO){
        Author authorUpdate = getAuthorRepository().findById(id).get();
        authorUpdate.setFirstName(authorDTO.getFirstName());
        authorUpdate.setLastName(authorDTO.getLastName());
        authorUpdate.setPatronymic(authorDTO.getPatronymic());
        authorUpdate.setBirthDate(authorDTO.getBirthDate());
        getAuthorRepository().save(authorUpdate);
    }

    @Transactional
    public void deleteById(Long id){
        getBookAuthorService().deleteByAuthorId(id);
        getAuthorRepository().deleteById(id);
    }
}
