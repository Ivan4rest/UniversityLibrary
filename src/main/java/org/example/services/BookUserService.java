package org.example.services;

import org.example.dto.BookDTO;
import org.example.entities.BookUser;
import org.example.repositories.BookRepository;
import org.example.repositories.BookUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookUserService {

    private BookUserRepository bookUserRepository;
    private BookRepository bookRepository;
    private BookService bookService;

    public BookUserRepository getBookUserRepository() {
        return bookUserRepository;
    }

    @Autowired
    public void setBookUserRepository(BookUserRepository bookUserRepository) {
        this.bookUserRepository = bookUserRepository;
    }

    public BookRepository getBookRepository() {
        return bookRepository;
    }

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public BookService getBookService() {
        return bookService;
    }

    @Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    public List<BookUser> getAll(){
        return getBookUserRepository().findAll();
    }

    public BookUser getById(Long id){
        return getBookUserRepository().findById(id).get();
    }

    public List<BookDTO> getBooksByUserId(String userUUID){
        List<BookDTO> bookDTOS = new ArrayList<>();
        List<Long> booksId = getBookUserRepository().getBooksIdByUserId(userUUID);
        for (int i = 0; i < booksId.size(); i++) {
            bookDTOS.add(getBookService().getById(booksId.get(i)));
        }
        return bookDTOS;
    }

    @Transactional
    public void add(String userUUID, Long bookId, Integer numberOfPage){
        BookUser bookUser = new BookUser(userUUID, getBookRepository().findById(bookId).get(), numberOfPage);
        getBookUserRepository().save(bookUser);
    }

    @Transactional
    public void update(String userUUID, Long bookId, Integer numberOfPage){
        Long bookUserId = getBookUserRepository().getByBookIdAndUserUUID(bookId, userUUID);
        BookUser bookUserUpdate = getBookUserRepository().findById(bookUserId).get();
        bookUserUpdate.setNumberOfPage(numberOfPage);
        getBookUserRepository().save(bookUserUpdate);
    }

    @Transactional
    public void deleteByUserIdAndBookId(String userUUID, Long bookId){
        getBookUserRepository().deleteByUserIdAndBookId(userUUID, bookId);
    }

    @Transactional
    public void deleteByUserId(String userUUID){
        getBookUserRepository().deleteByUserId(userUUID);
    }

    @Transactional
    public void deleteByBookId(Long id){
        getBookUserRepository().deleteByBookId(id);
    }
}
