package org.example.dto;

import lombok.Data;
import org.example.entities.Book;

import javax.persistence.criteria.CriteriaBuilder;
import java.sql.Date;
import java.util.List;

@Data
public class BookDTO {
    Long id;
    String title;
    Date publicationYear;
    String language;
    Long fileId;
    Long directoryId;
    Integer numberOfPages;
    List<Long> authorsId;
    List<Long> genresId;

    public BookDTO() {
    }

    public BookDTO(Long id, String title, Date publicationYear, String language, Long fileId, Long directoryId, Integer numberOfPages, List<Long> authorsId, List<Long> genresId) {
        this.id = id;
        this.title = title;
        this.publicationYear = publicationYear;
        this.language = language;
        this.fileId = fileId;
        this.directoryId = directoryId;
        this.numberOfPages = numberOfPages;
        this.authorsId = authorsId;
        this.genresId = genresId;
    }

    public BookDTO(Book book, List<Long> authorsId, List<Long> genresId) {
        this.id = book.getId();
        this.title = book.getTitle();
        this.publicationYear = book.getPublicationYear();
        this.language = book.getLanguage();
        this.fileId = book.getFileId();
        this.directoryId = book.getDirectoryId();
        this.numberOfPages = book.getNumberOfPages();
        this.authorsId = authorsId;
        this.genresId = genresId;
    }
}
