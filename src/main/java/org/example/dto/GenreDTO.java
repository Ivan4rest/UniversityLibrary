package org.example.dto;

import lombok.Data;
import org.example.entities.Genre;

import java.util.List;

@Data
public class GenreDTO {
    Long id;
    String name;
    List<Long> booksId;

    public GenreDTO() {
    }

    public GenreDTO(Long id, String name, List<Long> booksId) {
        this.id = id;
        this.name = name;
        this.booksId = booksId;
    }

    public GenreDTO(Genre genre, List<Long> booksId) {
        this.id = genre.getId();
        this.name = genre.getName();
        this.booksId = booksId;
    }
}
