package org.example.dto;

import lombok.Data;
import org.example.entities.Author;

import java.sql.Date;
import java.util.List;

@Data
public class AuthorDTO {
    Long id;
    String firstName;
    String lastName;
    String patronymic;
    Date birthDate;
    List<Long> booksId;

    public AuthorDTO() {
    }

    public AuthorDTO(Long id, String firstName, String lastName, String patronymic, Date birthDate, List<Long> booksId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.birthDate = birthDate;
        this.booksId = booksId;
    }

    public AuthorDTO(Author author, List<Long> booksId) {
        this.id = author.getId();
        this.firstName = author.getFirstName();
        this.lastName = author.getLastName();
        this.patronymic = author.getPatronymic();
        this.birthDate = author.getBirthDate();
        this.booksId = booksId;
    }
}
