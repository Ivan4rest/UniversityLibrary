package org.example.entities;

import org.example.entities.keys.BookGenreKey;

import javax.persistence.*;

@Entity
@Table(name = "book_genres")
public class BookGenre {

    @EmbeddedId
    private BookGenreKey id = new BookGenreKey();

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @MapsId("bookId")
    @JoinColumn(name = "book_id")
    private Book book;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @MapsId("genreId")
    @JoinColumn(name = "genre_id")
    private Genre genre;

    public BookGenre() {
    }

    public BookGenre(Book book, Genre genre) {
        this.book = book;
        this.genre = genre;
    }

    public BookGenreKey getId() {
        return id;
    }

    public void setId(BookGenreKey id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
}
