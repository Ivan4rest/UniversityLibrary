package org.example.entities;

import org.example.entities.keys.BookAuthorKey;

import javax.persistence.*;

@Entity
@Table(name = "book_authors")
public class BookAuthor {

    @EmbeddedId
    private BookAuthorKey id = new BookAuthorKey();

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @MapsId("bookId")
    @JoinColumn(name = "book_id")
    private Book book;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @MapsId("authorId")
    @JoinColumn(name = "author_id")
    private Author author;

    public BookAuthor() {
    }

    public BookAuthor(Book book, Author author) {
        this.book = book;
        this.author = author;
    }

    public void setId(BookAuthorKey id) {
        this.id = id;
    }

    public BookAuthorKey getId() {
        return id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
