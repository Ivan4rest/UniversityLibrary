package org.example.entities;

import javax.persistence.*;

@Entity
@Table(name = "book_users")
public class BookUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    @Column(name = "user_uuid")
    private String userUUID;

    @Column(name = "number_of_page")
    private Integer numberOfPage;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id")
    private Book book;

    public BookUser() {
    }

    public BookUser(String userUUID, Book book, Integer numberOfPage) {
        this.userUUID = userUUID;
        this.book = book;
        this.numberOfPage = numberOfPage;
    }

    public Long getId() {
        return id;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }

    public Integer getNumberOfPage() {
        return numberOfPage;
    }

    public void setNumberOfPage(Integer numberOfPage) {
        this.numberOfPage = numberOfPage;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
